
<?php echo get_the_post_thumbnail(array_pop(array_merge(array($post->ID), get_ancestors($post->ID,'page'))),'full', array( 'class' => 'img-responsive' )); ?>

<div class="container underliggende-wrap">
  <div class="row">
    <div class="col-md-12">
      <?php get_template_part('templates/page', 'header'); ?>

      <?php if ( function_exists('yoast_breadcrumb') ) {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
    </div>
  </div>

  <div class="row underliggende-content">
    <div class="col-md-8 col-xs-12">
      <?php while (have_posts()) : the_post(); ?>
        <?php get_template_part('templates/content', 'page'); ?>
      <?php endwhile; ?>
    </div>

    <div class="col-md-3 col-md-offset-1 col-xs-12">
      <?php dynamic_sidebar('sidebar-primary'); ?>
    </div>
  </div>
</div>
