<div class="top-bar-orange">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-xs-6"></div>
      <div class="col-md-2 col-xs-2 top-1"></div>
      <div class="col-md-2 col-xs-2 top-2"></div>
      <div class="col-md-2 col-xs-2 top-3"></div>
    </div>
  </div>
</div>

<div class="top-nav-wrap">
  <div class="container">
    <div class="row">
        <?php
        if (has_nav_menu('top_navigation')) :
          wp_nav_menu(['theme_location' => 'top_navigation', 'walker' => new wp_bootstrap_navwalker(), 'menu_class' => 'nav navbar-right top-nav']);
        endif;
        ?>
    </div>
  </div>
</div>

<header class="banner navbar navbar-default navbar-static-top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
        <span class="icon-bar icon-bar-top"></span>
        <span class="icon-bar icon-bar-middle"></span>
        <span class="icon-bar icon-bar-bottom"></span>
      </button>
      <a class="navbar-brand navbar-center" href="<?= esc_url(home_url('/')); ?>"><img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo-blade-technology.png" alt="" width="175" height="auto" /></a>
    </div>

    <nav class="collapse navbar-collapse" role="navigation">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new wp_bootstrap_navwalker(), 'menu_class' => 'nav navbar-nav navbar-right']);
      endif;
      ?>
    </nav>
  </div>
</header>
