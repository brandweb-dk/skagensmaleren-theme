<footer>
  <div class="content-info">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <?php dynamic_sidebar('footer-1'); ?>
        </div>

        <div class="col-md-4 footer-afdeling">
          <?php dynamic_sidebar('footer-2'); ?>
        </div>
      </div>

      <div class="row row-margin">
        <div class="col-md-3 footer-afdeling">
          <?php dynamic_sidebar('footer-3'); ?>
        </div>

        <div class="col-md-3 footer-afdeling">
          <?php dynamic_sidebar('footer-4'); ?>
        </div>

        <div class="col-md-3 footer-afdeling">
          <?php dynamic_sidebar('footer-5'); ?>
        </div>

        <div class="col-md-3 footer-afdeling">
          <?php dynamic_sidebar('footer-6'); ?>
        </div>
      </div>
    </div>
  </div>

  <div class="copyright">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <p>
            Alle rettigheder forbeholdes © 2016 Skagensmaleren
          </p>
        </div>
      </div>
    </div>
  </div>
</footer>
