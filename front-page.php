<div class="forside-slider">
  <?php layerslider(1) ?>
</div>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>


<div class="aoc-wrap">
    <div class="container aoc-wrap-padding">
        <div class="row">
            <div class="col-md-12 forside-intro">
              <h1>Specialister i renovering & behandling af beton- og ståloverflader</h1>
              <hr>
            </div>

            <div class="col-xs-12 col-sm-4">
                <div class="indholdskort">
                    <?php if( get_field('billede-v') ): ?>

                        <img src="<?php the_field('billede-v'); ?>" class="img-responsive"/>

                    <?php endif; ?>

                    <div class="indholdskort-content">
                        <h2><?php the_field('overskrift-v'); ?></h2>

                        <p><?php the_field('intro-v'); ?></p>

                        <a class="btn btn-aoc" href="<?php the_field('btn-link-v'); ?>"><?php the_field('btn-text-v'); ?></a>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4">
                <div class="indholdskort indholdskort-middle">
                    <?php if( get_field('billede-m') ): ?>

                        <img src="<?php the_field('billede-m'); ?>" class="img-responsive"/>

                    <?php endif; ?>

                    <div class="indholdskort-content">
                        <h2><?php the_field('overskrift-m'); ?></h2>

                        <p><?php the_field('intro-m'); ?></p>

                        <a class="btn btn-aoc" href="<?php the_field('btn-link-m'); ?>"><?php the_field('btn-text-m'); ?></a>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4">
                <div class="indholdskort">
                    <?php if( get_field('billede-h') ): ?>

                        <img src="<?php the_field('billede-h'); ?>" class="img-responsive"/>

                    <?php endif; ?>

                    <div class="indholdskort-content">
                        <h2><?php the_field('overskrift-h'); ?></h2>

                        <p><?php the_field('intro-h'); ?></p>

                        <a class="btn btn-aoc" href="<?php the_field('btn-link-h'); ?>"><?php the_field('btn-text-h'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
